<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About</title>

    <!-- Swiper CSS Link -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css"/>
    <!-- Font Awesome CDN Link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css">

    <!-- Custom CSS-File Link -->
    <link rel="stylesheet" href="./css/styles.css">

</head>
<body>
    

<!-- Header Section starts -->

<section class="header">

    <a href="home.php" class="logo">DomTrav</a>

    <nav class="navbar">
        <a href="home.php">Home</a>
        <a href="about.php">About</a>
        <a href="package.php">Package</a>
        <a href="book.php">Book</a>
    </nav>

    <div id="menu-btn" class="fas fa-bars"></div>

</section>

<!-- Header Section ends -->

<div class="heading" style="background:url(images/header-bg-1.png) no-repeat">
    <h1>About us</h1>
</div>

 <!-- About Section starts -->

<section class="about">

    <div class="iamge">
        <img src="images/about-img.jpg" alt="">
    </div>
    <div class="content">
        <h3>Why choose us?</h3>
        <p>Yous should choose us, because we offer you the best and cheapest Traveling Offers</p>
        <div class="icons-container">
            <div class="icons">
                <i class="fas fa-map"></i>
                <span>Top destinations</span>
            </div>
            <div class="icons">
                <i class="fas fa-hand-holding-usd"></i>
                <span>Affordable price</span>
            </div>
            <div class="icons">
                <i class="fas fa-headset"></i>
                <span>24/7 guide Service</span>
            </div>
        </div>
    </div>

</section>

 <!-- About Section ends -->
 <section class="reviews">

<h1 class="heading-title"> clients reviews </h1>

<div class="swiper reviews-slider">

   <div class="swiper-wrapper">

      <div class="swiper-slide slide">
         <div class="stars">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
         </div>
         <p>My journey was amazing and the guid was nice</p>
         <h3>Harry Miller</h3>
         <span>traveler</span>
         <img src="images/pic-1.png" alt="">
      </div>

      <div class="swiper-slide slide">
         <div class="stars">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
         </div>
         <p>My trip was nice but my guid was terrible</p>
         <h3>Jane Johnson</h3>
         <span>traveler</span>
         <img src="images/pic-2.png" alt="">
      </div>

      <div class="swiper-slide slide">
         <div class="stars">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
   </div>
         <p>All in all my Trip was good but the hotel was really bad</p>
         <h3>James Ferguson</h3>
         <span>traveler</span>
         <img src="images/pic-3.png" alt="">
      </div>

      <div class="swiper-slide slide">
         <div class="stars">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
         </div>
         <p>My trip was awesome and the guide and hotel were amazing</p>
         <h3>Sally Hills</h3>
         <span>traveler</span>
         <img src="images/pic-4.png" alt="">
      </div>

      <div class="swiper-slide slide">
         <div class="stars">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
         </div>
         <p>Although my filght was late the hotel, the guide and tip in gernal where ok</p>
         <h3>johnny Keyan</h3>
         <span>traveler</span>
         <img src="images/pic-5.png" alt="">
      </div>

      <div class="swiper-slide slide">
         <div class="stars">
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
            <i class="fas fa-star"></i>
         </div>
         <p>My trip was amzing, but the fligt was terrible.</p>
         <h3>Lui Long</h3>
         <span>traveler</span>
         <img src="images/pic-6.png" alt="">
      </div>

   </div>

</div>

</section>


<!-- Reviews Section starts -->


<!-- Reviews Section ends -->









<!-- Footer Section starts -->
<section class="footer">


    <div class="box-container">

        <div class="box">
            <h3>Quick Links</h3>
        <a href="home.php"> <i class="fas fa-angle-right"></i> Home</a>
        <a href="about.php"> <i class="fas fa-angle-right"></i> About</a>
        <a href="package.php"> <i class="fas fa-angle-right"></i> Package</a>
        <a href="book.php"> <i class="fas fa-angle-right"></i> Book</a>
        </div>

        <div class="box">
            <h3>Extra Links</h3>
         <a href="#"> <i class="fas fa-angle-right"></i> Ask Qustions</a>
         <a href="#"> <i class="fas fa-angle-right"></i> About us</a>
         <a href="#"> <i class="fas fa-angle-right"></i> Privacy Policy</a>
         <a href="#"> <i class="fas fa-angle-right"></i> Terms of use</a>
         </div>

         <div class="box">
            <h3>Contact Info</h3>
         <a href="#"> <i class="fas fa-phone"></i> +43 699 1070 6886</a>
         <a href="#"> <i class="fas fa-phone"></i> +43 512 3045 06</a>
         <a href="#"> <i class="fas fa-envelope"></i> domi.mit98@gmail.com</a>
         <a href="#"> <i class="fas fa-map"></i> Völs - Tyrol, Austria - 6176 </a>
       </div>

       <div class="box">
            <h3>Follow us</h3>
            <a href="#"> <i class="fab fa-facebook-f"></i> Facebook</a>
            <a href="#"> <i class="fab fa-twitter"></i> Twitter</a>
            <a href="#"> <i class="fab fa-linkedin"></i> LinkedIn</a>

       </div>
    </div>
    <div class="credit"> Created by <span>Dominik Mitterer</span> with love | All rights reserved</div>
</section>

<!-- Footer Section ends -->









<!-- Swiper Js Link -->
<script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>

<!-- Custom Js-File Link -->
<script src="js/script.js"></script>
</body>
</html>