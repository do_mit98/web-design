<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>

    <!-- Swiper CSS Link -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css"/>
    <!-- Font Awesome CDN Link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css">

    <!-- Custom CSS-File Link -->
    <link rel="stylesheet" href="./css/styles.css">

</head>
<body>
    

<!-- Header Section starts -->

<section class="header">

    <a href="home.php" class="logo">DomTrav</a>

    <nav class="navbar">
        <a href="home.php">Home</a>
        <a href="about.php">About</a>
        <a href="package.php">Package</a>
        <a href="book.php">Book</a>
    </nav>

    <div id="menu-btn" class="fas fa-bars"></div>
</section>

<!-- Header Section ends -->


<!-- Home Section starts -->



<section class="home">

   <div class="swiper home-slider">

      <div class="swiper-wrapper">

         <div class="swiper-slide slide" style="background:url(images/home-slide-1.jpg) no-repeat">
            <div class="content">
               <span>explore, discover, travel</span>
               <h3>travel arround the world</h3>
               <a href="package.php" class="btn">discover more</a>
            </div>
         </div>

         <div class="swiper-slide slide" style="background:url(images/home-slide-2.jpg) no-repeat">
            <div class="content">
               <span>explore, discover, travel</span>
               <h3>discover the new places</h3>
               <a href="package.php" class="btn">discover more</a>
            </div>
         </div>

         <div class="swiper-slide slide" style="background:url(images/home-slide-3.jpg) no-repeat">
            <div class="content">
               <span>explore, discover, travel</span>
               <h3>make your tour worthwhile</h3>
               <a href="package.php" class="btn">discover more</a>
            </div>
         </div>
         
      </div>

      <div class="swiper-button-next"></div>
      <div class="swiper-button-prev"></div>

   </div>

</section>

<!-- Home Section ends -->

<!-- Service Section starts -->

<section class="services">

<h1 class="heading-title"> Our Services </h1>

<div class="box-container">

<div class="box">
   <img src="images/icon-1.png" alt="">
   <h3>adventure</h3>
</div>

<div class="box">
   <img src="images/icon-2.png" alt="">
   <h3>tour guide</h3>
</div>

<div class="box">
   <img src="images/icon-3.png" alt="">
   <h3>trekking</h3>
</div>

<div class="box">
   <img src="images/icon-4.png" alt="">
   <h3>camp fire</h3>
</div>

<div class="box">
   <img src="images/icon-5.png" alt="">
   <h3>off road</h3>
</div>

<div class="box">
   <img src="images/icon-6.png" alt="">
   <h3>camping</h3>
</div>

</div>

</section>

<!-- Service Section ends -->

<!-- Home About Section starts -->

<section class="home-about">
   <div class="imag">
      <img src="images/about-img.jpg" alt="">   
   </div>

   <div class="content">
      <h3>about us</h3>
      <p>We are a Company which has been in the traveling buisness for many years</p>
      <a href="about.php" class="btn">Read more</a>

   </div>

</section>
<!-- Home About Section ends -->

<!-- Home Package Section starts -->

<section class="home-packages">

   <h1 class="heading-title"> our packages </h1>

   <div class="box-container">

      <div class="box">
         <div class="image">
            <img src="images/img-1.jpg" alt="">
         </div>
         <div class="content">
            <h3>adventure & tour</h3>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eos, sint!</p>
            <a href="book.php" class="btn">book now</a>
         </div>
      </div>

      <div class="box">
         <div class="image">
            <img src="images/img-2.jpg" alt="">
         </div>
         <div class="content">
            <h3>adventure & tour</h3>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eos, sint!</p>
            <a href="book.php" class="btn">book now</a>
         </div>
      </div>
      
      <div class="box">
         <div class="image">
            <img src="images/img-3.jpg" alt="">
         </div>
         <div class="content">
            <h3>adventure & tour</h3>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eos, sint!</p>
            <a href="book.php" class="btn">book now</a>
         </div>
      </div>

   </div>

   <div class="load-more"> <a href="package.php" class="btn">load more</a> </div>

</section>

<!-- Home Package Section ends -->

<!-- Home offer Section starts -->

<section class="home-offer">
   <div class="content">
      <h3>Upto 50% off</h3>
      <p> You can now save upto 50% on all the your bookings </p>
      <a href="book.php" class="btn">Book now</a>
   </div>
</section>


<!-- Home offer Section starts -->

<!-- Footer Section starts -->
<section class="footer">


    <div class="box-container">

        <div class="box">
            <h3>Quick Links</h3>
        <a href="home.php"> <i class="fas fa-angle-right"></i> Home</a>
        <a href="about.php"> <i class="fas fa-angle-right"></i> About</a>
        <a href="package.php"> <i class="fas fa-angle-right"></i> Package</a>
        <a href="book.php"> <i class="fas fa-angle-right"></i> Book</a>
        </div>

        <div class="box">
            <h3>Extra Links</h3>
         <a href="#"> <i class="fas fa-angle-right"></i> Ask Qustions</a>
         <a href="#"> <i class="fas fa-angle-right"></i> About us</a>
         <a href="#"> <i class="fas fa-angle-right"></i> Privacy Policy</a>
         <a href="#"> <i class="fas fa-angle-right"></i> Terms of use</a>
         </div>

         <div class="box">
            <h3>Contact Info</h3>
         <a href="#"> <i class="fas fa-phone"></i> +43 699 1070 6886</a>
         <a href="#"> <i class="fas fa-phone"></i> +43 512 3045 06</a>
         <a href="#"> <i class="fas fa-envelope"></i> domi.mit98@gmail.com</a>
         <a href="#"> <i class="fas fa-map"></i> Völs - Tyrol, Austria - 6176 </a>
       </div>

       <div class="box">
            <h3>Follow us</h3>
            <a href="#"> <i class="fab fa-facebook-f"></i> Facebook</a>
            <a href="#"> <i class="fab fa-twitter"></i> Twitter</a>
            <a href="#"> <i class="fab fa-linkedin"></i> LinkedIn</a>

       </div>
    </div>
    <div class="credit"> Created by <span>Dominik Mitterer</span> with love | All rights reserved</div>
</section>

<!-- Footer Section ends -->









<!-- Swiper Js Link -->
<script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>

<!-- Custom Js-File Link -->
<script src="js/script.js"></script>
</body>
</html>